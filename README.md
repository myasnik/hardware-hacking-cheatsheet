# Hardware Hacking Cheatsheet

[[_TOC_]]

## Disclaimer

- I'm a noob who's trying to learn this type of things so maybe something could be not 100% correct
- Sorry for bad english

## Notes

- **Follow the easyest path first methodology**
- Sometimes you will need to solder stuff; [here](./res/doc/FullSolderComic_EN.pdf) a quick and simple how to
    - You could have to solder wires directly on PCB vias ([video](https://www.youtube.com/watch?v=5P7bDsHVsV4&t=905))
        1. Scratch the surface of the PCB using a cutter until you can see the sparkling under the solder mask
        2. Scratch another time the surface using fiberglass pencil
        3. Clean the surface with some IPA and a cotton swab
        4. Apply a little bit of flux
        5. Tin the wire and solder
    - Notes
        - Always tin your soldering iron tip
        - Temperature: `250-350 C`
        - Don't touch PCB with hands

## Information gathering and first interaction

1. Look at the label on the back of the device and find
    - Model name
    - Serial number
    - The society who branded the device (may not be the one who build it)
2. Search on the internet using the information just gathered
    - Best websites containing information
        - [TechInfoDepot](http://en.techinfodepot.shoutwiki.com/wiki/Main_Page)
        - [OpenWRT](https://openwrt.org/)
    - Search for ..., this usually leads to a lot of information
        - FCC ID ([reference website](https://apps.fcc.gov/oetcf/tcb/reports/TCBSearch.cfm))
        - SOC name
        - Flash chip name and amount 
        - RAM chip name and amount
        - Other possible information sources
3. Open the device
    - Search for tutorials on how to open the device
    - Some devices could be glued to prevent opening, be gentle
    - Sometimes we have heatsinks that cover some part of the circuit, if possible remove it
4. Identify components
    - To make circuit names more readable
        - Use cotton + alcohol, when alcohol is dried cover the circuit in chalk, then clean it; now circuit name should be readable
        - Use a magnifying glass
    - Search for information and datasheets on the internet about these components; if nothing is found try on chinese search engines
        - [Baidu](https://www.baidu.com/baidu.html?from=noscript)
        - [Sogou](https://www.sogou.com/)
        - [Haosou](https://www.so.com/haosou.html?src=home)
    - **IMPORTANT**: finding a component that has `VCC` and `GND` well exposed is very useful 
5. Locate UART interface: more or less a TTY
    - Search on internet
    - Search on the PCB for `GND`, `IN` or `RX`, `OUT` or `TX` and `VCC`
    - Search for `3/4` pins on the PCB
        1. Find a reference to `GND`
            - Using previously found components
            - Usually metal plates are at `GND`
        2. Find a reference to `VCC`
            - Using previously found components
            - Search for capacitors, they usually have a point at `VCC`
        3. Test UART candidate pins filling the table below (bullet points are respectively each column of the table)
            1. Test UART pins resistance against `GND` (multimeter to resistance measurement, usually `200k`)
            2. Test UART pins resistance against `VCC` (multimeter to resistance measurement, usually `200kOhm`)
            3. Power up the device and test UART pins voltage against `GND` (multimeter to voltage measurement, usually `20V`)
            4. Power up the device and **DURING BOOT** test suspected `TX` UART pin voltage against `GND` (multimeter to voltage measurement, usually `20V`); if the voltage is oscillating then this pin is probably `TX` (because it's sending data)
            5. Power up the device and **DURING BOOT** test suspected `RX` UART pin voltage against `GND` (multimeter to voltage measurement, usually `20V`); if the voltage is stuck at `0` then this pin is probably `RX` (because it's waiting to receive data)
            - Table

                | PIN | `GND` resistance | `VCC` resistance | `V` | Notes |
                |-----|------------------|------------------|-----|-------|
                | 1   |                  |                  |     |       |
                | 2   |                  |                  |     |       |
                | 3   |                  |                  |     |       |
                | 4   |                  |                  |     |       |

                - Example

                    | PIN | `GND` resistance          | `VCC` resistance          | `V`    | Notes                     |
                    |-----|---------------------------|---------------------------|--------|---------------------------|
                    | 1   | `30kOhm`                  | `0Ohm`                    | `3.3V` | `VCC`                     |
                    | 2   | `4.7kOhm`                 | `34kOhm`                  | `3.3V` | `1.6-3.3V` on boot - `TX` |
                    | 3   | `INFOhm` (multimeter `1`) | `INFOhm` (multimeter `1`) | `3.3V` | `0V` on boot - `RX`       |
                    | 4   | `0Ohm`                    | `30kOhm`                  | `0V`   | `GND`                     |
    - Use Jtagulator
        1. Connect it to the computer (baud rate: `115200`)
        2. **IMPORTANT**: `H` is the print help function, use it everywhere
        3. Connect board `GND` to Jtagulator `GND`, board pins `1,2,3` to Jtagulator channels `1,2,3`
        4. `V`: set working voltage
        5. `U`: enter UART identification menu 
        6. `U`: start identification
        7. `Text string to output`: default
        8. `Starting channel`: channel where we put the pin `1` of the board
        9. `Ending channel`: channel where we put the pin `3` of the board
        10. `Ignore non-printable characters`: Yes
        11. Done!
    - **TODO**: - Use BurtleinaBoard + Busside
6. Connect through UART: use a serial adapter (UART -> USB) to connect to the board through a computer
    - Chosen serial adapter: [FT232H](https://it.aliexpress.com/item/32814913865.html) + [Focaccia Board](https://github.com/whid-injector/Focaccia-Board)  
    1. Choose the appropriate voltage (`3.3V` or `5V`), else the board or the serial adapter will be damaged
    2. Connect board `RX` to adapter `TX` and board `TX` with adapter `RX`
        - **NOTE**: usually connecting `VCC` pin is not needed
    3. Connect adapter to computer
        1. `sudo lsusb` to locate the adapter
        2. `ls -lart /dev` to locate all device files; our should be one of the last, usually `ttyUSB0`
        3. To access this device we must part of the `dialout` group (or be `root`), to check our groups `groups $USER`
        4. `screen /dev/ttyUSB0 $BAUDRATE` to attach to the TTY
            - `$BAUDRATE` can be one of the ones found [here](https://en.wikipedia.org/wiki/Serial_port)
            - Most common `$BAUDRATE`
                - `115200`
                - `9600`
                - `57600`
                - `38400`
                - `19200`
            - **IMPORTANT**: if we fuck up `$BAUDRATE` we could see gibberish or also **NOTHING**
            - `ctrl + a -> k -> y`: close screen
    - If the RX pin seems not to work (you type and press return but noting happens) it could be that the "return" value is wrong: `\r\n` or `\n`?
        - To solve this use `pyserial`, python's library for serial communication, example:
            ```python
            #!/usr/bin/env python3

            import serial

            ser = serial.Serial('/dev/ttyUSB0', 115200, tmieout = 0.1)
            ser.write(b"HELLO\r\n")
            ser.write(b"HELLO\n")
            ```
        - If the problem persist use a logic analyzer ([here](https://www.amazon.it/AZDelivery-Logic-Analyzer-compatibile-version/dp/B01MUFRHQ2/ref=sr_1_6?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=logic+analyzer&qid=1617875104&sr=8-6) a cheap one)
7. Locate JTAG interface
    - What is JTAG: the JTAG interface gives manufacturers a way to test the physical connections between pins on a chip. When electrical engineers talk about using JTAG to "debug" a chip, they are talking about something very different than traditional software debugging. They are talking about making sure pin A on chip A is physically connected to pin B on chip B, and that all those pins are functioning correctly. Since JTAG gives you direct hardware access to a device, it's also a fantastic tool for security research.
    - JTAG properties
        - Controllability: set to `0` or `1` internal bits
        - Observability: check the value of internal bits
        - ...so read/write EEPROM
        - In-circuit debugging: debug code on the circuit (using for example [OpenOCD](http://openocd.org/) and GDB)
    - Search on internet
    - Search on the PCB for `TCK`, `TDI`, `TDO`, `TMS` and `TRST` (optional)
        - `TCK` (Test Clock): the drummer, or metronome that dictates the speed of the controller. Voltage on this pin simply pulses up and down in a rhythmic, steady beat. On every "beat" of the clock, the controller takes a single action.
        - `TMS` (Test Mode Select): voltages on the Mode Select pin control what action JTAG takes. By manipulating the voltage on this pin, you tell JTAG what you want it to do.
        - `TDI` (Test Data-In): the pin that feeds data into the chip. The JTAG standard does not define protocols for communication over this pin. That is left up to the manufacturer. As far as JTAG is concerned, this pin is simply an ingress method for `1`s and `0`s to get into the chip. What the chip does with them is irrelevant to JTAG.
        - `TDO` (Test Data-Out): the pin for data coming out of the chip. Like the Data-In pin, communication protocols are not defined by JTAG.
        - `TRST` (Test Reset, optional): this signal is used to reset JTAG to a known good state.
    - Search for row of `5/6` pins or a double row of `10`, `12`, `14`, `20` pins on the PCB
        1. Find a reference to `GND`
            - Using previously found components
            - Usually metal plates are at `GND`
        2. Find a reference to `VCC`
            - Using previously found components
            - Search for capacitors, they usually have a point at `VCC`
        3. Test JTAG candidate pins filling the table below (bullet points are respectively each column of the table)
            1. Test JTAG pins resistance against `GND` (multimeter to resistance measurement, usually `200k`)
            2. Test JTAG pins resistance against `VCC` (multimeter to resistance measurement, usually `200kOhm`)
            3. Power up the device and test JTAG pins voltage against `GND` (multimeter to voltage measurement, usually `20V`)
            - Table

                | PIN | `GND` resistance | `VCC` resistance | `V` | Notes |
                |-----|------------------|------------------|-----|-------|
                | 1   |                  |                  |     |       |
                | 2   |                  |                  |     |       |
                | 3   |                  |                  |     |       |
                | 4   |                  |                  |     |       |
                | 5   |                  |                  |     |       |
                | 6   |                  |                  |     |       |
                | 7   |                  |                  |     |       |
                | 8   |                  |                  |     |       |
                | 9   |                  |                  |     |       |
                | 10   |                  |                  |     |       |
                | ...   |                  |                  |     |       |

                - Example

                    | PIN | `GND` resistance          | `VCC` resistance          | `V`    | Notes                  |
                    |-----|---------------------------|---------------------------|--------|------------------------|
                    | 1   | `1kOhm`                   | `1kOhm`                   | `0V`   |                        |
                    | 2   | `0Ohm`                    | `90Ohm`                   | `0V`   | `GND`                  |
                    | 3   | `INFOhm` (multimeter `1`) | `INFOhm` (multimeter `1`) | `2.1V` | High impedance, `TDO`? |
                    | 4   | `90Ohm`                   | `0Ohm`                    | `3.3V` | `VCC`                  |
                    | 5   | `4.7kOhm`                 | `4.7kOhm`                 | `3.3V` |                        |
                    | 6   | `INFOhm` (multimeter `1`) | `INFOhm` (multimeter `1`) | `0V`   | Not connected?         |
                    | 7   | `5.7kOhm`                 | `5.7kOhm`                 | `3.3V` |                        |
                    | 8   | `INFOhm` (multimeter `1`) | `INFOhm` (multimeter `1`) | `0V`   | Not connected?         |
                    | 9   | `4.7kOhm`                 | `4.7kOhm`                 | `3.3V` |                        |
                    | 10  | `0Ohm`                    | `90Ohm`                   | `0V`   | `GND`                  |

                    - Note: odd pins on the left, even on the right
                    - Compatible JTAG test site pinout found: [Altera Byteblaster](http://www.jtagtest.com/pinouts/altera_byteblaster)
        4. Compare found values with mostly used JTAG pinouts, available at [jtagtest](http://www.jtagtest.com/pinouts/)
    - Use Jtagulator
        1. Connect it to the computer (baud rate: `115200`)
        2. **IMPORTANT**: `H` is the print help function, use it everywhere
        3. Connect board `GND` to Jtagulator `GND`, board pins `1,2,3...` to Jtagulator channels `1,2,3...`
        4. `V`: set working voltage
        5. `J`: enter JTAG identification menu 
        6. There we have two options
            - `I`: identify with `IDCODE` scan, will not find `TDI` (fast), better if i have a lot pins to identify
            - `B`: identify with `BYPASS` scan, will find `TDI` (slow), better if i have less pins to identify
        7. `Starting channel`: channel where we put the pin `1` of the board
        8. `Ending channel`: channel where we put the pin `n` of the board
        9. `Already known pins`: No but it could speed the process up if we already know some pins
        10. Start and wait.. Done!
    - **TODO**: - Use BurtleinaBoard + Busside
    - **IMPORTANT**
        - JTAG may have been disabled (hardware, removing a resistor) so it is possible that what you find with the multimeter and Jtagulator isn't consistent; we can solve this putting a resistor of about `300Ohm` or `1kOhm` between this pin and `VCC`
        - JTAG may have been disabled (hardware, removing a resistor); this issue can be solved putting back this resistor or making a direct connection short circuiting the resistor pads
        - JTAG may have been disabled (software, setting some values)
        - JTAG may have been disabled (hardware, blowing a fuse... in this case there's no hope)
8. Connect through JTAG: use a "serial adapter" (JTAG -> USB) to connect to the board through a computer
    - Chosen "serial adapter": [FT232H](https://it.aliexpress.com/item/32814913865.html) + [Focaccia Board](https://github.com/whid-injector/Focaccia-Board)  
    1. Choose the appropriate voltage (`3.3V` or `5V`), else the board or the serial adapter will be damaged
    2. Using previously found JTAG pinout connect everything together 
    3. Keep UART connection open (as explained before) to interact with the device and look at how it behaves
    4. Run OpenOCD
        - First window (OpenOCD "server"): `openocd -f $FT232HCONFIGFILE -f $BOARDCONFIGFILE`
            - `$FT232HCONFIGFILE`: [Focaccia board reference](https://github.com/whid-injector/Focaccia-Board/blob/master/ft232h_jtag-swd.config)
            - `$BOARDCONFIGFILE`: the board's you are hacking config file (useful but maybe you won't have it, optional)
                - Notes
                    - Configuration files are inside `/usr/local`, maybe here you could find some useful `$BOARDCONFIGFILE`
                    - Else you could search on the internet
                    - Else you could write it yourself 
                    - **TODO** (write yourself)
        - Second window (OpenOCD "client"): `telnet localhost 4444`
            - Useful commands
                - `halt`: halt CPU (like freeze)
                    - MUST BE DONE BEFORE EVERY DEBUGGING OPERATION
                - `reset`: reset CPU
                - `reg`: read CPU registers
                - `flash info bank $BANKID` or `flash info $BANKID`: prints information about flash memory bank `$BANKID` (i think that banks = chunk of memory)
                - `flash list`: retrieves a list of associative arrays for each device that was declared using `flash bank` (in `$BOARDCONFIGFILE`), numbered from zero
                - `flash banks`: prints a one-line summary of each device that was declared using `flash bank` (in `$BOARDCONFIGFILE`), numbered from zero
                - `flash write_image erase "$BINTOWRITE" $ADDRTOSTART`: flash memory
                    - `$BINTOWRITE`: could be `bin` (binary), `ihex` (Intel hex), `elf` (ELF file), `s19` (Motorola s19), `mem`...
                    - `$ADDRTOSTART`: address where to start writing (i think that default is `0`)
                - `flash dump_image $OUTFILE $ADDRTOSTART $SIZETODUMP`: dump memory
                    - `$OUTFILE`: binary file where to save the dump
                    - `$ADDRTOSTART`: address where to start reading (i think that default is `0`)
                    - `$SIZETODUMP`: number of bytes to dump 
            - More here:
                - [OpenOCD PDF](http://openocd.org/doc/pdf/openocd.pdf)
                - [OpenOCD HTML](http://openocd.org/doc/html/)
            - **TODO**
9. Get the firmware and filesystem
    - Possibilities (firmware adn filesystem could be encrypted)
        - Download it from the manufacturer website
        - If only the device can download the firmware (through an update) sniff the net using wireshark to gather information
        - Reading the EEPROM directly using a flash chip programmer and a test clip
        - Bootloader dump command
            1. Analyze the boot log print on the UART interface
                - Information that could be print and we are interested in (values are examples but are explanatory of what we are looking for)
                    - Bootloader general info
                        - Search for bootloader name and version (ex: `U-Boot 1.1.3`)
                    - SOC info
                        - Additional boards info (wifi, ethernet...), they could mount their own bootloader
                        - SOC model (ex: `ASIC MT7621A...`)
                        - CPU frequency
                    - RAM info
                        - `mtd->writesize=2048`: page size (bytes)
                        - `mtd->oobsize=64`: data used for error correction (bytes)
                        - `devinfo.iowidth=8`: data written/read per operation (bytes)
                        - Amount of RAM
                    - EEPROM info
                        - `mtd->erasesize=131072`: EEPROM remaining writes? (more or less)
                    - OS Kernel info
                        - Look for bootloader loading info, here you could find info about the filesystem
                        - Look for buildroot version, this will help us emulating the circuit and doing various tests
                    - File system info
                        - Look for bootloader loading info and OS starting process, here you could find info about the filesystem
                    - EEPROM partitions
                        - Look for OS starting process, here you could find info about partitions of the EEPROM, their names, mount points and length in RAM
                        - If you see partitions duplicated is probably for firmware upgrades, you can guess why
                    - Init process info
                        - Search for `init started` or something like this, this probably will be near `BusyBox` string or something analogous
                    - Bootloader has CLI?
                        - **Search for bootloader menu**, here probably we could find the answer to this question
            2. Try to get a bootloader shell (automatically or through a menu print via UART)
            3. Explore the bootloader shell
                - `help` command is your friend
                - Try to find a way to dump the content of the memory, python is your friend
                - OOB data (error code correction) is not so useful to dump
            4. Dumped data analysis
                - Use `binwalk`, `file` and `hexdump -C` to check if the file dumped is ok or not and if is compressed or encrypted
                    - With `binwalk -E` we analyze the entropy of the file
                        - Entropy NEAR `1`: random, compressed or encrypted file
                        - Entropy BELOW `1`: normal executable or file
                - Use `binwalk -e` to extract identifiable segments of the file
            5. If `binwalk` doesn't fully understand the dumped image we could use the EEPROM partition table (if previously found) to manually split the dumped image in multiple useful images
                - `dd if=$IN_DUMPED_IMAGE of=$OUT_FILE bs=1024 skip=$BYTES_TO_SKIP_FROM_THE_START count=$HOW_MANY_BYTES_TO_WRITE`
                - `sha1sum`, `md5sum` or `binwalk -W -i` to compare images (if for example we think they could be the same image)
            6. The last operation could be executed multiple times depending on what the dumped image contains; if for example we have the kernel image we could extract its components another time with `binwalk` (or `dd` if we can find online how our specific kernel image is structured) to read the root filesystem
            7. Extract the filesystem
                - Example command (based on filesystem type): `fakeroot -s fakeroot.dat usquashfs -d squashfs-root u04-sqfs.dat` 
                    - `fakeroot`: create a fake root environment, useful to emulate file permissions, device files...
                        - `-s fakeroot.dat`: save fake root environment to further restore with command `fakeroot -i fakeroot.dat bash`
                    - `usquashfs`: extract `squashfs` filesystem (could be different in your case)
                        - `-d squashfs-root`: destination folder
                        - `u04-sqfs.dat`: filesystem image to extract

**TODO** SPI DUMP

**TODO** GDB ATTACH????

## Reverse engineering

1. Init process type and configuration files
    - Types
        - BSD style
            - Start executing scripts in
                - `/etc/rc`
                - `/etc/rc.local`
            - More recent
                - See in `/etc/rc.conf` for information
                - Execute `/etc/rc.d/`
        - System V (most popular)
            - BusyBox is started
            - Configuration files are in `/etc/inittab`
                - `runlevel`
                    - `1`: single user mode, root shell, no password, no deamon running
                    - `3`: text based multiuser mode, login prompt
                    - `5`: graphical login
                - Then there is a list of action done at init
            - Execute `/etc/init.d/`
        - Systemd (not used in embedded)
    - How to identify
        - Printed on boot
        - Analyze `/sbin/init` searching for information (above) identifying types
2. Interesting binaries and scripts
    - Look for interesting files started by init process and in general don't stop to the names, look in depth at what binary is being executed and analyze them; most interesting ones are the non-standard ones
    - Look for `factory mode` string; if we succeed in putting the device in factory mode (if it exists) then hacking it is a lot easier
    - Useful commands
        - Text editor
        - `grep`
        - `find`
        - `xargs`
        - `strings`

## Emulation environment

- Requirements
    - Know binary file CPU architecture
        - Trivial using `file` command
    - QEMU must support this architecture
- QEMU emulation (modes)
    - System mode: emulate the entire system
        - How to
            1. Locate QEMU executable format: `qemu-system-$PROCESSOR$ARCHITECTURE`
                - Example: `qemu-system-mipsel`
            2. If you know the processor family you could specify it to help QEMU better emulate the environment
                - To get a list of supported families: `$QEMUBIN -cpu help`
                - Is always better starting with a general family CPU, then if something doesn't work try to dig deeper and use specific CPU families
            3. We need kernel and root filesystem
                - Notes
                    - Device kernel is not good for missing drivers
                    - There is no standardization in IOT world
                        - Use kernel device tree: text file defining board drivers
                            - Kernel when booting will load this file and adapt generic drivers to the board in use
                            - Not so used
                    - SO.. REBUILD THE KERNEL AND FILESYSTEM
                1. Find kernel version, `libc` version and list of libraries used by the executable we are interested in (`readelf -d $EXECUTABLE`)
                    - Library version format: `libfoo.X.Y.Z` (`X.Y.Z` is the version)
                        - `X` incompatible ABI
                        - `Y` backward compatible ABI
                        - `Z` no ABI changes
                    - So we need `X.Y` equal to the original library
                        - Acceptable: same `X`, higher `Y`
                2. Build using a build system (select features and autotrack dependencies)
                    - Best build system choices
                        - The Yocto project
                        - Buildroot (best)
                        - OpenWRT build system

                    <!-- MISSING PART HOW TO BUILD FILESYSTEM AND KERNEL-->

            4. Start emulating
                - QEMU emulation script
                    ```bash
                    #!/bin/bash
                    # This script will build an environment without password for the user root
                    export QEMU_AUDIO_DRV="none" # ignore audio drivers

                    qemu-system-${PROCESSOR}${ARCHITECTURE} -M $CPUFAMILY \ # See point 2
                                                            -m $RAMSIZE \
                                                            -kernel $KERNELPATH \
                                                            -nographic \ # No GUI
                                                            -hda $FILESYSTEM \
                                                            -net nic,model=$NETCARDMODEL \ # Model of net card, driver must be included in kernel
                                                            -net user, hostfw=tcp::2222-:22, hostfw=tcp::9000-:9000 \ # 2222 as ssh and 9000 for GDB server
                                                            -no-reboot \ # Terminate the machine when is halted
                                                            -append "root=/dev/hda console=uart0" # Set root filesystem and console
                    ```
                    - If when executing a binary error about missing libs is print then set `LD_LIBRARY_PATH` as follow (inside the machine): `export LD_LIBRARY_PATH=/lib:/usr/lib:$PATHTOORIGINALFILESYSTEMLIBFOLDER`
                - We could also emulate the NAND EEPROM
                    ```sh
                    #!/bin/bash

                    # Part 1: Identify bytes for kernel module
                    modprobe nandsim first_id_byte=$FIRSTBYTE \
                                        second_id_byte=$SECONDBYTE \
                                        third_id_byte=$THIRDBYTE \
                                        fourth_id_byte=$FOURTHBYTE \
                                        cache_file=/root/nandsim.bin \
                                        parts=x,y,z,... # Define partitons size in number of erase blocks; the number of partitions depends on your device, partitions are usually print on boot

                    # Part 2: Erase partitions created (analyze EEPROM partitions)
                    flash_erase /dev/mtd0 0 8 
                    flash_erase /dev/mtd1 0 20
                    # ...

                    # Part 3: Load partitions dumped from device in the ones just created
                    nandwrite /dev/mtd0 part0.bin
                    nandwrite /dev/mtd1 part1.bin
                    # ...

                    # Part 4: Create mountpoint for filesystem and attach (if UBIFS)
                    mkdir /mnt/filesystem
                    ubiattach -O $N -m $MTDDVENUM -d $UBIDEVNUM

                    # Part 5: Mount
                    mount -tubifs /dev/ubi${UBIDEVNUM}_0 /mnt/filesystem
                    ```
                    1. Identify bytes for kernel module
                        - During boot usually information about NAND are print, look at it (above steps) and search for `NAND ID`, bytes print are in order first, second and fourth byte
                        - We could find those information also searching the datasheet of the EEPROM
                        - We could also use `writesize, oobsize, erasesize, iowidth` to find [here](http://www.linux-mtd.infradead.org/faq/nand.html#L_nand_nandsim) the correct command
                        - Else, trial and error
                    2. Analyze EEPROM partitions print on boot to find their sizes and names, the last number in the `flash_erase` command is their size correlated to the `erasesize` (same as `parts=x,y,z,...`)
                    3. Load partitions dumped from device in the ones just created
                    4. Create mountpoint for filesystem and attach
                        - `-O`: specify the volume id header offset, if wrong the system should tell you the right value anyway you could try different values like `512, 1024, 2048` (trial and error)
                        - `-m`: `mtd` device number (see point 3)
                        - `-d`: UBI device number (see point 5)
                    5. Mount
    - User mode: like wine, execute just a binary and "translate" it to our architecture
        - Notes
            - Not so stable
            - Can give strange results
        - How to
            1. Locate QEMU executable format: `qemu-$PROCESSOR$ARCHITECTURE`
                - Example: `qemu-mips64`
            2. If QEMU complains about missing interpreter pass the path of **THE FOLDER CONTAINING** this interpreter with `-L` (or see `man`)
                - To know what interpreter is used by an executable use `readelf -l $EXECUTABLE`
    - Virtualization mode: not interesting for us
- Kernel and root filesystem building using buildroot and docker
    - Our built kernel should have (in respect to the original kernel)
        - Same kernel version
        - Same libc version (`uClibc, uClibc-ng, musl, dietlibc...`)
        - Same libraries versions (of the executable we are interested in)
    1. Search the buildroot version that is the nearest to versions of our device
        - Sometimes during boot/exploring the dumped memory we could find the version of buildroot used (if the device was built using buildroot)
    2. Find a linux version compatible with the buildroot version found and create a docker container, [here](https://github.com/digiampietro/hacking-gemtek/blob/master/docker/Dockerfile) a sample dockerfile (packages are important to run buildroot)
    3. Download selected buildroot version from [here](https://buildroot.org/downloads/) and put it inside the docker container shared folder
    4. Run and switch to the docker container
    5. Extract buildroot and `make manual` to create buildroot's manual
    6. Using `make help` buildroot prints all supported devices (boards); `make $YOURBOARDNAME` to create a buildroot configuration file of your board
    7. `make menuconfig` (text based) or `make xconfig` (GUI) to select kernel modules to add to our build, we'll use `make xconfig`
        - Here there is an example/guideline but you will need to discover by yourself which modules in particular you will need to run your applications
        - Cheat: `Edit->Find` to search for modules
        - Options
            - `Target options`
                - Check `Show options and packages that are deprecated or obsolete`
                - Check `Build packages with debugging symbols` with highest `debug level`
                - Check `Strip command for binaries on target` to `None`
                - Check `GCC optimization level` to `0`
            - `Toolchain`
                - Check `Toolchain type` to `Buildroot toolchain`
                - Check `Kernel headers` to `Manually specified linux version`
                - Check `Custom kernel headers series` to `$DEVICEKERNELVERSION`
                - Set `Linux version` to `$DEVICEKERNELVERSION`
                - Check `C library` to `$DEVICECLIBRARY`
                - Check `$DEVICECLIBRARY version` to `$DEVICECLIBRARY $DEVICELIBRARYVERSION`
                - Check `Enable large files`
                - Check `Enable IPv6`
                - Check `Enable RPC`
                - Check `Enable WCHAR`
                - Check `Thread library implementation` to `linuxthreads`
                - Check `Thread library debugging`
                - Check `Build cross gdb for the host`
                - Check `TUI support`
                - Check `Python support`
                - Check `GDB debugger version` to `$LATESTGDBVERSION`
            - `System configuration`
                - Check `Passwords encoding` to `MD5`
                - Check `Init system` to `$DEVICEINITSYSTEM` (or `BusyBox`)
                - Check `/dev management` to `Dynamic using devtmpfs only`
                - Check `/bin/sh` to `Busybox default shell`
                - Check `Install timezone info`
            - `Kernel`
                - Set `Kernel version` to `$DEVICEKERNELVERSION`
                - Check `Kernel binary format` to `vmlinux`
            - `Target packages`
                - `Compressors and decompressors`
                    - `bzip2` and `xz-utils`
                - `Debugging profiling and benchmark`
                    - Check `gdb` and `full debugger`
                - `Development tools`
                    - What you will need
                - `Filesystem and flash utilities`
                    - `mtd, jffs2 and ubi/ubifs tools` (or what you will need)
                - `Libraries`
                    - In general what you need (suggestions below)
                    - `Crypto`
                        - `libsha1`
                        - `libssh2`
                        - `openssl`
                    - `JSON/XML`
                        - `expat`
                        - `json-c`
            - `Networking applications`
                - `rsync` and what you need
            - `Shell and utilities`
                - `file`
            - `Filesystem images`
                - `ext2`
            - `Host utilities` (not about target device, we are talking about host device here)
                - `host mtd, jffs2 and ubi/ubifs tools`
                - `host util-linux`
        - Remember to SAVE
    8. To permanently save the configuration just defined use `make savedconfig`
    9. Configure the kernel with `make linux-menuconfig` (text based) or `make linux-xconfig` (GUI), here we'll use CLI version
        - Here there is an example/guideline but you will need to discover by yourself which modules in particular you will need to run your applications
        - Options
            - `Kernel type` -> `Preemption model (Preemptible Kernel (Low-Latency Desktop))` -> `Preemptible Kernel (Low-Latency Desktop)`
            - `Kernel type` -> `Device drivers` -> `Memory technology device (MTD) support` -> `NAND device support` -> `Support for NAND flash simulator`
            - `Kernel type` -> `Device drivers` -> `Memory technology device (MTD) support` -> `UBI - Unsorted block images` -> `Enable UBI`
            - `File systems` -> `Miscellaneous filesystem` -> `JFFS2 support`
            - `File systems` -> `Miscellaneous filesystem` -> `UBIFS filesystem support`
        - `Save`
    10. Configure `uClibc` (or your c lib) using `uclibc-menuconfig` (as always)
        - Enable debugging: `Development/Debugging options` -> `Enable debugging symbols`, if this doesn't work (compiling errors) then `Development/Debugging options` -> `(Wall) compiler warnings` -> add `-Wall -ggdb -g3`
            - `-ggdb`: provides debugging info to use with GDB
            - `-g3`: provide extra debugging information 
        - `Save`
        - Enable features as in our device (trial and error, if you get errors investigate and then rebuild with features needed)
    11. `make`, if problems iterate back
        - Possible compilation errors
            - `Need to use -fPIC`
                - Add `--enable-shared` in kernel modules (point 7) under `Toolchain` -> `Additional gcc options` or patch buildroot
    - Saving configuration files for buildroot with git
        - Eternal tree configuration (tree view of files to be understand by buildroot) (`br2`)
            ```
            +-- board/
            |   +-- <company>/ (not always used)
            |       +-- <boardname>/
            |           +-- linux.config
            |           +-- busybox.config
            |           +-- kernel-defconfig (kernel config file)
            |           +-- <other configuration files>
            |           +-- post_build.sh (executed just before building the image, useful to copy root filesystem into the image)
            |           +-- post_image.sh
            |           +-- rootfs_overlay/ (everythin here will be copied in the final image)
            |           |   +-- etc/
            |           |   +-- <some file>
            |           +-- patches/
            |               +-- foo/
            |               |   +-- <some patch>
            |               +-- libbar/
            |                   +-- <some other patches>
            |
            +-- configs/
            |   +-- <boardname>_defconfig (buildroot config for our board)
            |   +-- uClibc.config (optional)
            +-- patches/
            |   +-- (here patches to be applied)
            |
            +-- Config.in (if using a br2-external tree)
            +-- external.mk (if using a br2-external tree)
            +-- external.desc (if using a br2-external tree)
            ```
        - To use an external tree invoke buildroot like this: `make BR2_EXTERNAL=$PATHTOEXTTREE $COMMAND`
        - To save buildroot config to our external tree: `make BR2_EXTERNAL=$PATHTOEXTTREE savedefconfig`
        - To save kernel config to our external tree: `make BR2_EXTERNAL=$PATHTOEXTTREE linux-update-defconfig`
        - To save uClibc config to our external tree: `make BR2_EXTERNAL=$PATHTOEXTTREE BR2_UCLIBC_CONFIG=$PATHWHERETOSAVEUCLIBCCONFIG uclibc-update-defconfig`

## Sources, credits and thanks

- Thanks to [Valerio Di Giampietro](https://va.ler.io/) ([@valerio](https://twitter.com/valerio)) for his incredible youtube tutorial [channel](https://www.youtube.com/channel/UCoyNuc5bJ-z4X-6OlpNtJUw) about hardware hacking, everything written here is at most took by these videos.
- Thanks to [Luca Bongiorni](https://lockpicking.club/) ([@LucaBongiorni](https://twitter.com/LucaBongiorni)) for his precious advices and hardware tools.
- Thanks to mightyohm.com for [Soldering comics](https://mightyohm.com/files/soldercomic/FullSolderComic_EN.pdf)
- Thanks to the Reddit [hardwarehacking](https://www.reddit.com/r/hardwarehacking) community for helping me
    - [[Noob] Direct PCB soldering (maybe?)](https://www.reddit.com/r/hardwarehacking/comments/lrbtaa/noob_direct_pcb_soldering_maybe/?utm_medium=android_app&utm_source=share)
- Thanks to [Andrew Paul](https://www.youtube.com/channel/UCtfcWatnWeEY_vq_UOL4PDQ) for vias soldering tutorial
- [Buildroot manual](https://buildroot.org/downloads/manual/manual.html)
- [JTAG explained](https://blog.senr.io/blog/jtag-explained)
- [OpenOCD - Flash commands](http://openocd.org/doc/html/Flash-Commands.html)
- [OpenOCD + JTAG information](https://www.allaboutcircuits.com/technical-articles/getting-started-with-openocd-using-ft2232h-adapter-for-swd-debugging/)
- [Hardware hacking cheatheet - Little PDF](https://github.com/arunmagesh/hw_hacking_cheatsheet/blob/master/cheatsheet_0.1.pdf)
- [OpenOCD](http://openocd.org/)
